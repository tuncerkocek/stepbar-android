package tuncer.kocek.be.stepbarsample

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import tuncer.kocek.be.stepbar.StepBarLayout

class MainActivity : AppCompatActivity() {
    private var stepBarLayout: StepBarLayout? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        stepBarLayout = findViewById(R.id.step)
        stepBarLayout!!.stepBar.setStepTitle(0, "Title")
        stepBarLayout!!.stepBar.setStepTitle(1, "Title")
        stepBarLayout!!.stepBar.setStepTitle(2, "Title")
        stepBarLayout!!.stepBar.setStepDescription(0, "Explanation here")
        stepBarLayout!!.stepBar.setStepDescription(1, "Explanation here")
        stepBarLayout!!.stepBar.setStepDescription(2, "Explanation here")
        var titleColor: EditText = findViewById(R.id.titleColor)
        titleColor.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setTitleColor(Color.parseColor((s.toString())))
                } catch (e: Exception) {

                }

            }

        })

        var circleTextColor: EditText = findViewById(R.id.circleTextColor)
        circleTextColor.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setCircleTextColor(Color.parseColor((s.toString())))
                } catch (e: Exception) {

                }

            }


        })
        var lineColor: EditText = findViewById(R.id.lineColor)
        lineColor.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setLineColor(Color.parseColor((s.toString())))
                } catch (e: Exception) {

                }
            }

        })

        var filledCircle: EditText = findViewById(R.id.filledCircle)
        filledCircle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setFilledCircleColor(Color.parseColor((s.toString())))
                } catch (e: Exception) {

                }
            }

        })

        var descriptionTextColor: EditText = findViewById(R.id.descriptionTextColor)
        descriptionTextColor.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setDescriptionTextColor(Color.parseColor((s.toString())))
                } catch (e: Exception) {

                }
            }

        })
        var animationDuration: EditText = findViewById(R.id.animationDuration)
        animationDuration.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    stepBarLayout!!.stepBar.setAnimationDuration(s.toString().toInt())
                } catch (e: Exception) {

                }
            }

        })
        var showMore: Spinner = findViewById(R.id.showMore)
        showMore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    stepBarLayout!!.stepBar.showInformationOnClick(true)
                } else {
                    stepBarLayout!!.stepBar.showInformationOnClick(false)
                }
            }

        }

        var activateAnimation: Spinner = findViewById(R.id.activateAnimation)
        activateAnimation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    stepBarLayout!!.stepBar.setActivateAnimation(true)
                } else {
                    stepBarLayout!!.stepBar.setActivateAnimation(false)
                }
            }

        }

        var selectedStep: EditText = findViewById(R.id.selectedStep)
        selectedStep.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(!s.toString().isEmpty())
                    stepBarLayout!!.stepBar.setSelectedStep((s.toString().toInt()))

            }
        })

        var maxStep: EditText = findViewById(R.id.maxStep)
        maxStep.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(!s.toString().isEmpty())
                stepBarLayout!!.stepBar.setMaxStep((s.toString().toInt()))

            }
        })
    }
}
