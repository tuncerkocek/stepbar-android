# Stepbar
[Apache2](https://www.apache.org/licenses/LICENSE-2.0)

**Stepbar** - An Android custom step bar fully customizable with description step(image, title,explanation)
<p align="center"><img src="Screen/stepbar.png" /></p>

## Gradle

```java

UNDER BUILD.GRADLE(PROJECT)
In :

allprojects {
    repositories {
        google()
        jcenter()
    }
}
add following link:
        maven {
           ...
        }
        
        so it will looks like after the adding
        allprojects {
            repositories {
                google()
                jcenter()
                maven {
                    url  "https://dl.bintray.com/tuncerkocek/Stepbar"
                }
        }
        
Then in app build.gradlle 

dependencies {
   implementation 'com.tuncer.kocek:stepbar:0.0.1@aar'
}
```

## Usage

* In XML layout: 

```xml
   <tuncer.kocek.be.stepbar.StepBarLayout
                android:id="@+id/step"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
               >
                
                or if you want only the stepbar 
    
     <tuncer.kocek.be.stepbar.StepBar
                android:id="@+id/step"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:filledCircleColor="@android:color/holo_blue_bright">
        </tuncer.kocek.be.stepbar.StepBar>
                
            
            **Remember** Don't add padding, the widget is already centered. Use layout gravity center to center it in layout or margin !
* In JAVA : 

```Java
    StepBarLayout stepBarLayout=new StepBarLayout(this);

# Visual style Customization





* All customizable attributes:

```xml
 <declare-styleable name="stepbarAttr">
        <attr name="titleColor" format="color" />
        <attr name="circleTextColor" format="color" />
        <attr name="lineColor" format="color" />
        <attr name="filledCircleColor" format="color" />
        <attr name="descriptionTextColor" format="color" />
        <attr name="showMore" format="boolean" />
        <attr name="activateAnimation" format="boolean" />
        <attr name="selectedStep" format="integer" />
        <attr name="maxStep" format="integer" />
        <attr name="animationDuration" format="integer" />
    </declare-styleable>
```

## Sample
* Clone the repository and check out the `app` module.

## Licence
Copyright 2018 Tuncer Kocek

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.