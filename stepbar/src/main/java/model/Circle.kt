package model

/**
 * Créer par Tuncer Kocek  10/26/2018
 */
class Circle(var x: Float, var y: Float, var radius: Float) {

    /**
     * if the position x is in circle, return true,
     * false otherwise
     */
    operator fun contains(x: Int): Boolean {
        return x >= this.x - radius
    }

    /**
     * If point is in circle return true, false otherwise
     */
    operator fun contains(p: Point): Boolean {
        val distance = Math.sqrt(Math.pow(p.x - x, 2.0) + Math.pow(p.y - y, 2.0))
        return if (distance <= radius) {
            true
        } else false
    }
}
