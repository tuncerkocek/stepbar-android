package tuncer.kocek.be.stepbar;

import android.animation.Animator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import model.Circle;
import model.Point;

import java.util.ArrayList;

/**
 * Créer par Tuncer Kocek  10/26/2018
 */
public class StepBar extends View {
    /**
     * Maximal step
     */
    private int maxStep = 3;
    /**
     * All step circle
     */
    private ArrayList<Circle> circles = new ArrayList<>();
    /**
     * Paint
     */
    private Paint paint;
    /**
     * Used to calculate the text width and height
     */
    private Rect textBound = new Rect();
    /**
     * All Titles of all step
     */
    private String[] stepTitle = new String[3];
    /**
     * All step description
     */
    private String[] stepDescription = new String[3];
    /**
     * All step image description
     */
    private Drawable[] stepImage = new Drawable[3];
    /**
     * Supplementary padding for circle space
     */
    private float padding = 45 / 2;
    /**
     * The current step
     */
    private int selectedStep = 0;
    /**
     * Draw line to x position between all circle
     */
    private int lineTo = 0;

    /**
     * THe animator lineTo value
     */
    private PropertyValuesHolder propertyRadius;

    /**
     * Animator
     */
    private Animator animator;
    /**
     * Contains all reached circle with @lineTo in animaton
     * Then in ondraw we put the color to circle
     */
    private ArrayList<Integer> animatedList = new ArrayList<>();
    /**
     * Color of title
     */
    private int titleColor = Color.WHITE;
    /**
     * Color of step number texts
     */
    private int circleTextColor = Color.WHITE;

    /**
     * If user touch screen or not
     */
    private boolean screenTouched = false;
    /**
     * if screen touched x y position in one circle, this attribut contains the number of circle that contains the position
     */
    private int clickedCircle;
    /**
     * if description is currentlyDescriptionIsShowed or not
     */
    private boolean currentlyDescriptionIsShowed = false;
    /**
     * The view width
     */
    private int width = 500;
    /**
     * The view height
     */
    private int height = 175;
    /**
     * The radius of all circle
     */
    private float circleRadius = 45;
    /**
     * If animation is already launched
     */
    private boolean animationIsLaunched = true;

    /**
     * Animation is activated or not
     */

    private boolean activateAnimation = true;

    private int animationDuration = 2000;
    /**
     * Filled line color
     */
    private int lineColor = getResources().getColor(R.color.grey);
    /**
     * The color when circle is reached
     */
    private int filledCircleColor = Color.RED;
    /**
     * The descriptionColor
     */
    private int descriptionTextColor = Color.BLACK;

    /**
     * The imageview to show when clicked
     */
    private ImageView imageDescription;
    /**
     * The textview title to show when clicked
     */
    private TextView title;
    /**
     * The description Textview to show when clicked
     */
    private TextView descriptionTextView;
    /**
     * If we want to show description on click
     */
    private boolean showMore = true;

    /**
     * Handler to unshown the description
     */
    final Handler handler = new Handler();
    /**
     * Listener for change and ontouch
     */
    private StepBarListener listener;

    public StepBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StepBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public StepBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
        createEmptyCircle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = 175;
        setMeasuredDimension(width, height);
        positionningEmptyCircle();
        startAnimation();
    }

    /**
     * Positionning the empty circle depending on max step
     */
    private void positionningEmptyCircle() {
        float spaceBetween = (width - (padding * 2)) / maxStep;
        for (int i = 0; i < maxStep; i++) {
            circles.get(i).setX((spaceBetween * i) + circles.get(i).getRadius() + spaceBetween / 2);
            circles.get(i).setY(height - circles.get(i).getRadius() - 75);
        }
    }

    /**
     * Initialize max step number circle
     */
    private void createEmptyCircle() {
        circles.clear();
        for (int i = 0; i < maxStep; i++) {
            circles.add(new Circle(0, 0, circleRadius));
        }
    }

    /**
     * startAnimation
     */
    private void startAnimation() {
        if (activateAnimation) {
            propertyRadius = PropertyValuesHolder.ofInt("x", (int) circles.get(0).getX(), ((int) circles.get(selectedStep).getX()));
            animator = new ValueAnimator();
            ((ValueAnimator) animator).setValues(propertyRadius);
            animator.setDuration(animationDuration);
            ((ValueAnimator) animator).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    animatedList.clear();
                    if (animationIsLaunched&&(lineTo < (int)circles.get(selectedStep).getX()||lineTo>(int)circles.get(selectedStep).getX())) {
                        lineTo = (int) animation.getAnimatedValue("x");
                        for (int i = 0; i < circles.size(); i++) {
                            if (circles.get(i).contains(lineTo) && !animatedList.contains(i)) {
                                animatedList.add(i);
                            }
                        }

                        invalidate();
                    } else {
                        noAnimation();
                    }
                }
            });

            animator.start();
        } else {
            noAnimation();
        }
    }

    /**
     * Reach directly the x point to finish
     */
    private void noAnimation() {
        lineTo = (int) circles.get(selectedStep).getX();
        animatedList.clear();
        for (int i = 0; i <= selectedStep; i++) {
            animatedList.add(i);
        }
        invalidate();
    }

    /**
     * Draw the title
     * @param canvas
     * @param i
     */
    private void drawText(Canvas canvas, int i) {

        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(8);
        paint.setColor(circleTextColor);
        paint.setTextSize(circles.get(0).getRadius() - 10);
        paint.setTextAlign(Paint.Align.CENTER);
        String number = String.valueOf((i + 1));
        paint.getTextBounds(number, 0, number.length(), textBound);
        canvas.drawText(number, (int) circles.get(i).getX(), (int) circles.get(i).getY() + (textBound.height() / 2), paint);
        if (i < stepTitle.length) {
            paint.setColor(titleColor);
            paint.getTextBounds(number, 0, number.length(), textBound);
            if (stepTitle[i] != null) {
                canvas.drawText(stepTitle[i], circles.get(i).getX(), (int) circles.get(i).getY() + textBound.height() + circles.get(i).getRadius() + 5, paint);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBackgroundLine(canvas);

        drawEmptyElement(canvas);

        drawFilledElement(canvas);
        drawDescription();


    }

    /**
     * Draw the empty circle
     *
     * @param canvas Where to draw the circle
     */
    private void drawEmptyElement(Canvas canvas) {
        for (int i = 0; i < circles.size(); i++) {
            paint.setColor(lineColor);
            Circle circle = circles.get(i);
            canvas.drawCircle(circle.getX(), circle.getY(), circle.getRadius(), paint);
            drawText(canvas, i);
        }
    }

    /**
     * Draw the selected element (line,circle,...)
     *
     * @param canvas Where to draw all the element
     */
    private void drawFilledElement(Canvas canvas) {
        paint.setColor(filledCircleColor);
        canvas.drawLine(circles.get(0).getX(), circles.get(0).getY(), lineTo, circles.get(0).getY(), paint);
        for (int i = 0; i < animatedList.size(); i++) {
            Circle circle = circles.get(animatedList.get(i));
            paint.setColor(filledCircleColor);
            canvas.drawCircle(circle.getX(), circle.getY(), circle.getRadius(), paint);
            drawText(canvas, i);
        }
    }

    /**
     * Show the description
     */
    private void drawDescription() {
        if (screenTouched && stepDescription[clickedCircle] != null) {

            String text = stepDescription[clickedCircle];
            descriptionTextView.setTextColor(descriptionTextColor);
            title.setTextColor(descriptionTextColor);
            descriptionTextView.setText(text);
            descriptionTextView.setVisibility(VISIBLE);
            if (stepImage[selectedStep] != null) {
                imageDescription.setImageDrawable(stepImage[selectedStep]);
            }
            title.setText(stepTitle[clickedCircle]);
            imageDescription.setVisibility(VISIBLE);
            title.setVisibility(VISIBLE);
            if (!currentlyDescriptionIsShowed) {
                currentlyDescriptionIsShowed = true;

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        screenTouched = false;
                        currentlyDescriptionIsShowed = false;
                        invalidate();
                    }
                }, 5000);
            }
        } else {
            descriptionTextView.setVisibility(GONE);
            imageDescription.setVisibility(GONE);
            title.setVisibility(GONE);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (showMore) {
            for (int i = 0; i < circles.size(); i++) {
                if (circles.get(i).contains(new Point(event.getX(), event.getY()))) {
                    screenTouched = true;
                    clickedCircle = i;

                    invalidate();
                    if (listener != null) {
                        listener.onTouched(this, clickedCircle);
                    }
                    return true;
                }
            }
        }
        screenTouched = false;
        return true;
    }

    /**
     * Draw the unselected line that pass under circle
     *
     * @param canvas Where to draw all the element
     */
    private void drawBackgroundLine(Canvas canvas) {
        paint.setColor(lineColor);
        paint.setStrokeWidth(15);
        canvas.drawLine(circles.get(0).getX() + circles.get(0).getRadius(), circles.get(0).getY(), circles.get(circles.size() - 1).getX(), circles.get(circles.size() - 1).getY(), paint);
    }

    /**
     * Change current step
     *
     * @param selectedStep The new step
     */
    public void setSelectedStep(int selectedStep) {
        if (selectedStep < maxStep && selectedStep >= 0) {
            this.selectedStep = selectedStep;
            if (animator != null && animator.isStarted()) {
                animator.end();
            }
            animationIsLaunched = true;
            startAnimation();
            if (listener != null) {
                listener.onCurrentStepChanged(this, selectedStep);
            }
        }
    }

    /**
     * Next step
     */
    public void nextStep() {
        if (selectedStep < maxStep - 1) {
            setSelectedStep(selectedStep + 1);
        }
    }

    /**
     * Previous step
     */
    public void previousStep() {
        if (selectedStep > 0) {
            setSelectedStep(selectedStep - 1);
        }
    }

    /**
     * Step title
     *
     * @param stepNumber the step number that we want change or add title
     * @param title      The title of the corresponded step
     */
    public void setStepTitle(int stepNumber, String title) {
        if (stepNumber < circles.size() && stepNumber < stepTitle.length) {
            stepTitle[stepNumber] = title;
        }
        invalidate();
    }

    /**
     * Set the description to the right step
     *
     * @param stepNumber  The step number to set
     * @param description The new description
     */
    public void setStepDescription(int stepNumber, String description) {
        if (stepNumber < circles.size() && stepNumber < stepTitle.length) {
            stepDescription[stepNumber] = description;
        }
        invalidate();
    }

    /**
     * Set new image to step
     *
     * @param stepNumber The nuùber of step
     * @param drawable   The drawable to set for explanation
     */
    public void setDescriptionImage(int stepNumber, Drawable drawable) {
        if (stepNumber < circles.size() && stepNumber < stepTitle.length) {
            stepImage[stepNumber] = drawable;
        }
        invalidate();
    }

    /**
     * Change the title text color
     *
     * @param titleColor The new title color
     */
    public void setTitleColor(int titleColor) {
        this.titleColor = titleColor;
        invalidate();
    }

    /**
     * Change the title text color
     *
     * @param circleTextColor The new circle color
     */
    public void setCircleTextColor(int circleTextColor) {
        this.circleTextColor = circleTextColor;
        invalidate();
    }

    /**
     * Set the element to shw more explanation
     *
     * @param image       The explanation image view
     * @param description The description textview
     * @param title       The title textview
     */
    public void setViewGroup(ImageView image, TextView description, TextView title) {
        descriptionTextView = description;
        imageDescription = image;
        this.title = title;
        invalidate();
    }

    /**
     * Change max step
     *
     * @param maxStep The new max step
     */
    public void setMaxStep(int maxStep) {
        stepTitle = new String[maxStep];
        this.maxStep = maxStep;
        stepDescription = new String[maxStep];
        createEmptyCircle();
        positionningEmptyCircle();
        invalidate();
    }

    /**
     * If we show or not the explanation square
     *
     * @param showMore True if we want to show, false otherwise
     */
    public void showInformationOnClick(boolean showMore) {
        this.showMore = showMore;
    }

    /**
     * activate or not the animation
     *
     * @param activateAnimation True if we want animation false other wise
     */
    public void setActivateAnimation(boolean activateAnimation) {
        this.activateAnimation = activateAnimation;
    }

    /**
     * Animation duration
     *
     * @param animationDuration The new duration
     */
    public void setAnimationDuration(int animationDuration) {
        this.animationDuration = animationDuration;
    }

    /**
     * Add new listener
     * @param listener The listener
     */
    public void setListener(StepBarListener listener) {
        this.listener = listener;
    }

    public void setDescriptionTextColor(int descriptionTextColor) {
        this.descriptionTextColor = descriptionTextColor;
        invalidate();
    }

    /**
     * Set color to the filled circle with the new color
     * @param filledCircleColor
     */
    public void setFilledCircleColor(int filledCircleColor) {
        this.filledCircleColor = filledCircleColor;
        invalidate();
    }

    /**
     * Set color to filled line
     * @param lineColor
     */
    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
        invalidate();
    }


}
