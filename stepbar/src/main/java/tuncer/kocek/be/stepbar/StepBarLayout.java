package tuncer.kocek.be.stepbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Créer par Tuncer Kocek  10/30/2018
 */
public class StepBarLayout extends LinearLayout {
    private LayoutInflater inflater;
    private StepBar stepBar;
    private TextView description;
    private TextView title;
    private ImageView image;
    private LinearLayout descriptionLayout;

    public StepBarLayout(Context context) {
        super(context);
        initView(context, null);
    }

    public StepBarLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public StepBarLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public StepBarLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.stepbar, null));
        stepBar = findViewById(R.id.stepBars);

        description = findViewById(R.id.description);
        title = findViewById(R.id.title);
        image = findViewById(R.id.imageDescription);
        stepBar.setViewGroup(image, description, title);
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(
                    attrs, R.styleable.stepbarAttr);
            stepBar.setMaxStep(array.getInteger(R.styleable.stepbarAttr_maxStep, 3));
            stepBar.setSelectedStep(array.getInteger(R.styleable.stepbarAttr_selectedStep, 0));
            stepBar.setActivateAnimation(array.getBoolean(R.styleable.stepbarAttr_activateAnimation, true));
            stepBar.showInformationOnClick(array.getBoolean(R.styleable.stepbarAttr_showMore, true));
            stepBar.setDescriptionTextColor(array.getColor(R.styleable.stepbarAttr_descriptionTextColor, Color.BLACK));
            stepBar.setFilledCircleColor(array.getColor(R.styleable.stepbarAttr_filledCircleColor, Color.RED));
            stepBar.setLineColor(array.getColor(R.styleable.stepbarAttr_lineColor, getResources().getColor(R.color.grey)));
            stepBar.setCircleTextColor(array.getColor(R.styleable.stepbarAttr_circleTextColor, Color.WHITE));
            stepBar.setTitleColor(array.getColor(R.styleable.stepbarAttr_titleColor, Color.BLACK));
            stepBar.setAnimationDuration(array.getInteger(R.styleable.stepbarAttr_animationDuration, 2000));
            array.recycle();
        }


    }

    /**
     * Return the stepbar
     *
     * @return The stepbar
     */
    public StepBar getStepBar() {
        return stepBar;
    }

    /**
     * Change the description layout background
     *
     * @param color The new background color
     */
    public void setDescriptionBackgroundColor(int color) {
        descriptionLayout.setBackgroundColor(color);
    }
}
