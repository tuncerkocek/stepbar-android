package tuncer.kocek.be.stepbar;

/**
 * Créer par Tuncer Kocek  10/30/2018
 */
public interface StepBarListener {
    void onTouched(StepBar stepBar, int touchedCircle);

    void onCurrentStepChanged(StepBar stepBar, int newStep);
}
